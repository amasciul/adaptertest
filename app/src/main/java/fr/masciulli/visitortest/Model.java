package fr.masciulli.visitortest;

interface Model {
    ViewModel createViewModel(ViewModelFactory factory);
}
