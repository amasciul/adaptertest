package fr.masciulli.visitortest;

class ImageViewModel extends ViewModel {
    ImageViewModel(Model model) {
        super(model);
    }

    @Override
    public int type() {
        return R.layout.item_image;
    }
}
