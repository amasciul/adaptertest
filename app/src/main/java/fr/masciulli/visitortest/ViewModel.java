package fr.masciulli.visitortest;

abstract class ViewModel {
    private final Model model;

    public ViewModel(Model model) {
        this.model = model;
    }

    abstract int type();

    Model getModel() {
        return model;
    }
}
