package fr.masciulli.visitortest;

class HeaderViewModel extends ViewModel {

    HeaderViewModel(Header model) {
        super(model);
    }

    @Override
    public int type() {
        return R.layout.item_header;
    }
}
