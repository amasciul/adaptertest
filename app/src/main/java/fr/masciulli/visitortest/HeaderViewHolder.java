package fr.masciulli.visitortest;

import android.view.View;
import android.widget.TextView;

class HeaderViewHolder extends ModelViewHolder {
    private final TextView textView;

    HeaderViewHolder(View root) {
        super(root);
        textView = (TextView) root.findViewById(R.id.text);
    }

    @Override
    public void bind(ViewModel viewModel) {
        Header header = (Header) viewModel.getModel();
        textView.setText(header.getTitle());
    }
}
