package fr.masciulli.visitortest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

class VisitorAdapter extends RecyclerView.Adapter<ModelViewHolder> {
    private final List<ViewModel> viewModels = new ArrayList<>();

    VisitorAdapter(List<Model> models) {
        ViewModelFactory factory = new ViewModelFactory();
        for (Model model : models) {
            viewModels.add(model.createViewModel(factory));
        }
    }

    @Override
    public ModelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        switch (viewType) {
            case R.layout.item_header:
                return new HeaderViewHolder(root);
            case R.layout.item_image:
                return new ImageViewHolder(root);
            default:
                throw new IllegalStateException("Unknown type");
        }
    }

    @Override
    public void onBindViewHolder(ModelViewHolder holder, int position) {
        holder.bind(viewModels.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return viewModels.get(position).type();
    }

    @Override
    public int getItemCount() {
        return viewModels.size();
    }
}
