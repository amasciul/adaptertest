package fr.masciulli.visitortest;

class Header implements Model {
    private final String title;

    Header(String title) {
        this.title = title;
    }

    @Override
    public ViewModel createViewModel(ViewModelFactory factory) {
        return factory.createViewModel(this);
    }

    String getTitle() {
        return title;
    }
}
