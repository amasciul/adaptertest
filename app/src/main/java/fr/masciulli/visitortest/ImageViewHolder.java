package fr.masciulli.visitortest;

import android.view.View;
import android.widget.ImageView;

public class ImageViewHolder extends ModelViewHolder {
    private final ImageView imageView;

    ImageViewHolder(View root) {
        super(root);
        imageView = (ImageView) root.findViewById(R.id.image);
    }

    @Override
    public void bind(ViewModel viewModel) {
        Image image = (Image) viewModel.getModel();
        imageView.setImageDrawable(imageView.getResources().getDrawable(image.getImage()));
    }
}
