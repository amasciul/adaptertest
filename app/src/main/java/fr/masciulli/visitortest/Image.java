package fr.masciulli.visitortest;

class Image implements Model {
    private final int image;

    Image(int image) {
        this.image = image;
    }

    @Override
    public ViewModel createViewModel(ViewModelFactory factory) {
        return factory.createViewModel(this);
    }

    int getImage() {
        return image;
    }
}
