package fr.masciulli.visitortest;

class ViewModelFactory {
    ViewModel createViewModel(Image image) {
        return new ImageViewModel(image);
    }

    ViewModel createViewModel(Header header) {
        return new HeaderViewModel(header);
    }
}
