package fr.masciulli.visitortest;

import android.support.v7.widget.RecyclerView;
import android.view.View;

abstract class ModelViewHolder extends RecyclerView.ViewHolder {
    public ModelViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(ViewModel viewModel);
}
