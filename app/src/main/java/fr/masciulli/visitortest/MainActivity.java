package fr.masciulli.visitortest;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recycler = (RecyclerView) findViewById(R.id.recycler);

        List<Model> models = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            models.add(new Header("Section" + i));
            models.add(new Image(R.mipmap.ic_launcher));
            models.add(new Image(R.mipmap.ic_launcher));
            models.add(new Image(R.mipmap.ic_launcher));
            models.add(new Image(R.mipmap.ic_launcher));
        }

        recycler.setAdapter(new VisitorAdapter(models));
        recycler.setLayoutManager(new LinearLayoutManager(this));
    }
}
